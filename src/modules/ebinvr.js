import {log} from './utils.js';

import * as THREE from 'three-full';
import 'three-full/sources/vr/WebVR';

class EbinVR {
  /**
   * Constructs a new EbinVR instance
   * @param {Object} [options] -
   * @param {Object} [options.scene] - A Three.js scene object
   * @param {Object} [options.renderer] - A Three.js renderer object
   */
  constructor(options) {
    const defaults = {
      scene: new THREE.Scene(),
      renderer: new THREE.WebGLRenderer()
    };
    Object.assign(this, defaults, options);
  }

  /**
   * Main initializer for EbinVR
   */
  init() {
    this.renderer.vr.enabled = true;
    this.initScene();
    this.camera.position.z = 1000;

    this.renderer.setAnimationLoop( this.animate.bind(this) );
    this.attachToDOM();
    this.attachListeners();
    this.fitToView();

    log('Well whaddya know.. It works!');
  }

  initScene() {
    const geometry = new THREE.BoxGeometry(700, 700, 700, 10, 10, 10);
    const material = new THREE.MeshBasicMaterial({
      color: 0x88ff88,
      wireframe: true
    });
    const items = {
      cube: new THREE.Mesh(geometry, material),
      camera: new THREE.PerspectiveCamera(70, this.getFOV(), 1, 10000)
    };
    this.cube = items.cube;
    this.camera = items.camera;
    Object.values(items).forEach(item => this.scene.add(item));
  }

  /**
   * Calculates the Field of View
   * @returns {Number} - The calculated FOV
   */
  getFOV() {
    return window.innerHeight / window.innerWidth;
  }

  /**
   * Attaches EbinVR event listeners
   */
  attachListeners() {
    window.addEventListener('resize', this.onWindowResize.bind(this), false);
  }

  /**
   * Attaches EbinVR DOM elements
   */
  attachToDOM() {
    const bodyEls = [
      this.renderer.domElement,
      THREE.WebVR.createButton(this.renderer)
    ];
    bodyEls.forEach(el => document.body.appendChild(el));
  }

  /**
   * Main EbinVR Animation function
   */
  animate() {
    this.cube.rotation.x += 0.01;
    this.render();
  }

  /**
   * Renders the EbinVR scene
   */
  render() {
    this.renderer.render( this.scene, this.camera );
  }

  /**
   * Global 'resize' event handler
   */
  onWindowResize() {
    this.fitToView();
  }

  /**
   * Resizes the renderer to fit into view.
   */
  fitToView() {
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize( window.innerWidth, window.innerHeight );
  }
}

export default EbinVR;
