/**
 * Console.log shorthand helper
 * @param  {...any} args - The data to be logged
 * @returns {void}
 */
const log = function log(...args) {
  window.console.log(...args);
};

module.exports = {
  log
};
