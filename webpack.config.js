var path = require('path');

module.exports = {
  mode: 'development',
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'main.js'
  },
  devtool: 'eval-source-map',
  plugins: [],
  devServer: {
    disableHostCheck: true,
    contentBase: path.resolve(__dirname),
    compress: true,
    port: 8000,
  }
};
